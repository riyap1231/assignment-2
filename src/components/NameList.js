import { useState } from "react";

export const NameList = (props) => {
  const names = ['riya','anu','rajvi','ravina']
  return (
    <div>
      {names.map((name)=>{
        return <p key={name}>{name}</p>
      })}
    </div>
  );
};
