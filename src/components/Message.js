import { useState } from "react";

export const Message = (props) => {
  const [message, setMessage] = useState("Welcome Riya");
  return (
    <div>
      <h1>{message}</h1>
      <button
        onClick={() => {
          setMessage("Welcome Panchal");
        }}
      >
        Click me
      </button>
    </div>
  );
};
